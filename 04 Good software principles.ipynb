{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Some tenets of good software"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We're going to take another small break from learning new syntax or technical tools to think about how we can write better code.\n",
    "\n",
    "There are numerous blogs, articles and books about these topics but I wanted to pick out a few that I think are particularly worthwhile."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Don't repeat yourself"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One of the first and easiest to apply is that of avoiding repetition. This is often referred to as the [DRY principle](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself).\n",
    "\n",
    "The most direct application of this is that of using functions. If you're ever in the situation where you are copying and pasting code, it's probably worth stopping and thinking \"should I move this into a function?\".\n",
    "\n",
    "The advantage of the DRY principle is that by avoiding duplication you make maintenance easier. If you want to update, change or fix something you only need to do it in one place.\n",
    "\n",
    "As with all of these \"rules\", it is not absolute. It is not evil to duplicate code but instead use this as a guiding principle to keep in mind while writing and keep asking the question."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Make your code easy to use correctly and hard to use incorrectly"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is a principle that was [coined by Scott Meyers in 2004](https://doi.org/10.1109/MS.2004.29). The idea being that if you are writing code which will be used by others (functions, classes etc.) or writing user-interfaces (website, apps etc.) then you should endeavour to make the correct use of your product the \"easy path\".\n",
    "\n",
    "As an example, let's look at a function which calculated the distance in kilometres between a given latitude/longitude pair and Bristol:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from math import sin, cos, sqrt, atan2, radians\n",
    "\n",
    "def distance_from_bristol(lon, lat):\n",
    "    \"\"\"\n",
    "    Given a longitude and latitude in degrees,\n",
    "    return the distance in km from Bristol.\n",
    "    \"\"\"\n",
    "    lon, lat = radians(lon), radians(lat)\n",
    "    bristol_lat = radians(51.4539886)\n",
    "    bristol_lon = radians(-2.6068184)\n",
    "    dlon = lon - bristol_lon\n",
    "    dlat = lat - bristol_lat\n",
    "    a = sin(dlat / 2)**2 + cos(bristol_lat) * cos(lat) * sin(dlon / 2)**2\n",
    "    c = 2 * atan2(sqrt(a), sqrt(1 - a))\n",
    "    return 6373.0 * c"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When we come to use this function, we call it by passing the two values:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "7638.924775713775"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "lat_london = 51.5006895\n",
    "lon_london = -0.1245838\n",
    "\n",
    "distance_from_bristol(lat_london, lon_london)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That number is far too big! What happened?\n",
    "\n",
    "The problem here is that the function expected the arguments to be passed in a longitude first and the latitude but we passed them the other way around. This function is *easy to use incorrectly*.\n",
    "\n",
    "To help solve this, Python has a feature where you can specify that certain arguments *must* be passed in as named arguments. This is done by setting a literal `*` as a parameter and then all following parameters must only be passed by name:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "def distance_from_bristol(*, lon, lat):  #  ← the only line that has changed\n",
    "    \"\"\"\n",
    "    Given a longitude and latitude in degrees,\n",
    "    return the distance in km from Bristol.\n",
    "    \"\"\"\n",
    "    lon, lat = radians(lon), radians(lat)\n",
    "    bristol_lat = radians(51.4539886)\n",
    "    bristol_lon = radians(-2.6068184)\n",
    "    dlon = lon - bristol_lon\n",
    "    dlat = lat - bristol_lat\n",
    "    a = sin(dlat / 2)**2 + cos(bristol_lat) * cos(lat) * sin(dlon / 2)**2\n",
    "    c = 2 * atan2(sqrt(a), sqrt(1 - a))\n",
    "    return 6373.0 * c"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now when we try to call the function without specifying which argument is which, we get an error:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [
    {
     "ename": "TypeError",
     "evalue": "distance_from_bristol() takes 0 positional arguments but 2 were given",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mTypeError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-4-343c08b29905>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[0;32m----> 1\u001b[0;31m \u001b[0mdistance_from_bristol\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mlat_london\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mlon_london\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;31mTypeError\u001b[0m: distance_from_bristol() takes 0 positional arguments but 2 were given"
     ]
    }
   ],
   "source": [
    "distance_from_bristol(lat_london, lon_london)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once we are explicit, it works correctly:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "172.03101346881488"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "distance_from_bristol(lat=lat_london, lon=lon_london)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is now *harder to use incorrectly*.\n",
    "\n",
    "There's still the issue that it's very easy to pass in the latitude and longitude in the wrong units. A potential solution to this would be to create a `Point` class which encode within it whether the units are degree or radians and require users of that class to specify then putting in values or removing them."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Zen of Python"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python has a document, called *The Zen of Python*, which describes what it considers the core principles for writing good Python code. It is available as [Python Enhancement Proposal 20](https://www.python.org/dev/peps/pep-0020/) and is also available by importing the special `this` module.\n",
    "\n",
    "It's worth having a read through as almost all of these ideas apply to programming in general."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The Zen of Python, by Tim Peters\n",
      "\n",
      "Beautiful is better than ugly.\n",
      "Explicit is better than implicit.\n",
      "Simple is better than complex.\n",
      "Complex is better than complicated.\n",
      "Flat is better than nested.\n",
      "Sparse is better than dense.\n",
      "Readability counts.\n",
      "Special cases aren't special enough to break the rules.\n",
      "Although practicality beats purity.\n",
      "Errors should never pass silently.\n",
      "Unless explicitly silenced.\n",
      "In the face of ambiguity, refuse the temptation to guess.\n",
      "There should be one-- and preferably only one --obvious way to do it.\n",
      "Although that way may not be obvious at first unless you're Dutch.\n",
      "Now is better than never.\n",
      "Although never is often better than *right* now.\n",
      "If the implementation is hard to explain, it's a bad idea.\n",
      "If the implementation is easy to explain, it may be a good idea.\n",
      "Namespaces are one honking great idea -- let's do more of those!\n"
     ]
    }
   ],
   "source": [
    "import this"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Of particular note are:\n",
    "\n",
    "<dl>\n",
    "<dt>Readability counts</dt>\n",
    "<dd>When writing code, don't just think about how it will be interpreted by the computer, also consider your fellow human. Code will be read many more time than it is written so optimise for understandability.</dd>\n",
    "    \n",
    "<dt>Errors should never pass silently.</dt>\n",
    "<dd>This is the logic behind Python's use of exceptions. You can't ignore an error unless you explicitly decide to. This is in contrast to common techniques in use in languages like C where a function might return a <code>0</code> if it was successful or a <code>1</code> otherwise and it would be up to the person calling the function to remember to check the value themselves.</dd>\n",
    "    \n",
    "<dt>There should be one — and preferably only one — obvious way to do it.</dt>\n",
    "<dd>This goes hand-in-hand with the idea of making your code easy to use correctly and hard to use incorrectly. Provide a simple and consistent interface to your users, and don't display unnecessary complexity.</dd>\n",
    "</dl>\n",
    "\n",
    "To explain the \"unless you're Dutch\" comment, the Zen of Python was written by Tim Peters in the early days of Python and this is intended as a friendly jab at the creator of Python [Guido van Rossum](https://en.wikipedia.org/wiki/Guido_van_Rossum) who is Dutch.\n",
    "\n",
    "There's a lot of good advice in there and I recommend coming back and giving it a read every now and again. Despite it being over 20 years old, it's still completely relevant."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Testable code is better code"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The one rule that I've found to be the most useful when deciding what is \"good code\" is the question \"how easy is this to test?\". If there's one thing to take away from this course, I'd say it should be this.\n",
    "\n",
    "You will find that in the process of thinking about how to make your code more easily testable you'll make it more modular, composable and with better-defined interfaces. All of which make it cleaner, easier to understand and more maintainable.\n",
    "\n",
    "Testing therefore has the double benefit of both giving confidence that your code is correct and making the code better along the way."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
